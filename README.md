Exercice d'algorithmie sur l'affectation de planning
====================================================

L'objectif est de répartir les volontaires dont la liste est définie dans le fichier volunteers.json sur les différents créneaux définis dans slots.json.

# Exercice 1
Dans cet exercice, on ne tiendra pas compte de la problématique des préférences.

## Calculer pour chaque créneaux le nombre de volontaires potentiels.

En appuyant sur le bouton "Recherche", on ajoutera sur chaque ligne des créneaux ", maximum : X" où X est le nombre potentiel de volontaires.

## Mettre en évidence les "créneaux à problème"
Mettre en rouge les créneaux où maximum < minimum en ajoutant la class "alert".

## Répartir les volontaires
Répartir les volontaires dans chaque créneaux sans prendre en compte les priorités.

# Exercice 2

## 1ère préférence 
Refaire l'exercice 1 en n'utilisant que la première préférence de chaque volontaire

## 2ème et 3ème préférence 
Puis itérer sur les créneaux incomplets avec la 2ème puis la 3ème préférence.


import '../css/main.css';
import {displayData, computeData} from './display.js'

function searchClick()
{
    computeData();
}

// main
window.onload = function() {
    displayData();

    let search = document.getElementById("searchBtn");
    search.addEventListener("click", searchClick)
};
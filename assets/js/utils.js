export {setText, addText}

function setText(id, text)
{
    let result = document.getElementById(id);
    result.innerHTML = text;
}

function addText(id, text)
{
    let result = document.getElementById(id);
    result.innerHTML += text;
}
import {addText, setText} from './utils.js'
import volunteers from '../json/volunteers.json';
import tasks from '../json/tasks.json';
import {computeMax, assignUsers} from '../js/compute.js'
export {displayData, computeData}

// time slots
function displaySlot(slot)
{
    const begin = new Date(slot.begin);
    const end = new Date(slot.end);

    return begin.toLocaleDateString() + ' ' + begin.toLocaleTimeString() + ' - ' + end.toLocaleTimeString();
}

function displaySlots(slots)
{
    return slots.reduce((acc, slot) => acc + displaySlot(slot) + ' ', '');
}

// preferences
function displayPreferences(preferences)
{
    let res = '';
    preferences.forEach((value, key) => res += `${key > 0 ? ', ' : ''}${key + 1} : ${value}`);
    return res;
}

// volunteers
function displayVolunteer(x)
{
    addText('volunteers', `<p>${x.name} : ${displaySlots(x.slots)}, préférences : ${displayPreferences(x.preferences)}</p>`)
}

function displayUsers(users)
{
    if(users.length === 0) {
        return "Pas de bénévoles";
    }
    return users.reduce((acc, user) => acc + user + ' ', '');
}

// task slots
function displayTask(x)
{
    addText('tasks', `<p>${x.name} : ${displaySlot(x.slot)}, mininum : ${x.min}, maximum : ${x.max}, users : ${displayUsers(x.users)}</p>`)
}

// display volunteers
function displayVolunteers(list)
{
    setText('volunteers', '');
    addText('volunteers', '<h2>Volontaires</h2>');
    list.forEach(element => displayVolunteer(element));
}

// display volunteers
function displayTasks(list)
{
    setText('tasks', '');
    addText('tasks', '<h2>Créneaux</h2>');
    list.forEach(e => displayTask(e));
}

// entry points

function displayData()
{
    displayVolunteers(volunteers);
    displayTasks(tasks);

}

function computeData()
{
     setText("result", "c'est bon !");
     //addText("result", isSlotIntoSlot({"begin":"2021-08-05T10:00:00+02:00", "end":"2021-08-05T12:00:00+02:00"}, {"begin":"2021-08-05T11:00:00+02:00", "end":"2021-08-05T14:00:00+02:00"}) ? 'dedans' : 'pas dedans');

     computeMax(tasks, volunteers);
     assignUsers(tasks, volunteers);
     displayTasks(tasks);
     displayVolunteers(volunteers);
}

export {computeMax, isSlotIntoSlot, assignUsers}

// slot : un object avec un champ begin et un champ end
function isSlotIntoSlot(childSlot, parentSlot)
{
    if(childSlot.begin < parentSlot.begin) {
        return false;
    }
    if(childSlot.end > parentSlot.end) {
        return false;
    }
    return true;
}

function updateSlots(volunteer, slot)
{
    let slots = [];
    volunteer.slots.forEach(s => {
        if(isSlotIntoSlot(slot, s)) {
            if (s.begin < slot.begin) {
                slots.push({"begin": s.begin, "end": slot.begin});
            }
            if (s.end > slot.end) {
                slots.push({"begin": slot.end, "end": s.end});
            }
        } else {
            slots.push(s);
        }
    })
    volunteer.slots = slots;
}

function volunteerHasTaskslot(volunteer, taskSlot)
{
    let result = volunteer.slots.filter(s => {return isSlotIntoSlot(taskSlot.slot, s)});

    return result.length > 0;
}

function addVolunteer(volunteer, taskSlot)
{
    if(volunteerHasTaskslot(volunteer, taskSlot)) {
        if(taskSlot.users.length < taskSlot.min) {
            taskSlot.users.push(volunteer.name);
            volunteer.assigned++;
            updateSlots(volunteer, taskSlot.slot);
        }
    }
}

function computeMax(tasks, volunteers)
{
    // parcours toutes les taches et regarde si un volontaire a un créneau compatible
    tasks.forEach(task => {

        //task.max = volunteers.reduce((acc, v) => {return acc + volunteerHasTaskslot(v, task) ? 1 : 0}, 0);
        let max = 0;

        // faire un sort
        volunteers.forEach(v => {
            if(volunteerHasTaskslot(v, task)) {
                max++;
            }
        })
        task.max = max;
    })
}

function getPreferenceValue(preferences, preference)
{
    let index = preferences.indexOf(preference);

    if(index === -1) {
        return 10;
    }
    return index;
}

function sortByProperty(volunteers, preference) {
    let result = volunteers.sort(function (a, b) {
        let valueA = getPreferenceValue(a.preferences, preference);
        let valueB = getPreferenceValue(b.preferences, preference);

        if(a.assigned > b.assigned) return 1
        if(b.assigned > a.assigned) return -1

        if (valueA > valueB) {
            return 1;
        }
        if (valueA < valueB) {
            return -1;
        }
        return 0;
    });

    return result;
}

function assignUsers(tasks, volunteers)
{
    // parcours toutes les taches et regarde si un volontaire a un créneau compatible
    tasks.forEach(task => {
        // faire un sort
        sortByProperty(volunteers, task.name);

        volunteers.forEach(v => {
            addVolunteer(v, task);
        })
    })
}